export interface EventBusMessage {
    sender: string;
    receiver: string;
    message: string;
    timestamp: Date;
}
