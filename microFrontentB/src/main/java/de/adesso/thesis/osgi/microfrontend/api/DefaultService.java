package de.adesso.thesis.osgi.microfrontend.api;

import de.adesso.thesis.osgi.microfrontend.dto.DummyDto;

import java.util.List;

public interface DefaultService {
    List<DummyDto> getAllDummys();
}
