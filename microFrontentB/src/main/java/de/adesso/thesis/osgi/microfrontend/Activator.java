package de.adesso.thesis.osgi.microfrontend;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

    private BundleContext context;

    public void start(BundleContext context) {
        this.context = context;

    }

    public void stop(BundleContext context) {

    }
}
