import { BrowserModule } from '@angular/platform-browser';
import {ApplicationRef, Injector, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {createCustomElement} from '@angular/elements';
import {environment} from '../environments/environment';
import {CommonModule} from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule
  ],
  providers: [],
  entryComponents: [AppComponent]
})
export class AppModule {
  constructor(private injector: Injector) {}
  ngDoBootstrap(app: ApplicationRef) {

    if (environment.production) {
      
      //*** IMPORTANT ***
      // this is the component tag name generatated by maven build
      // DO NOT CHANGE THIS VALUE !
      let componentTagName = '_MICRO_FRONTEND_NAME_';
      const customElementName = componentTagName.toLocaleLowerCase();
      const appElement = createCustomElement(AppComponent, { injector: this.injector });

      // if the Web Component is already known, dont define it again.
      if (!customElements.get(customElementName)) {
        customElements.define(customElementName, appElement);
      }
    } else {
      app.bootstrap(AppComponent);
    }
  }
}
