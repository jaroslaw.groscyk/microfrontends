#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.api;

import ${package}.dto.DummyDto;

import java.util.List;

public interface DefaultService {
    List<DummyDto> getAllDummys();
}
