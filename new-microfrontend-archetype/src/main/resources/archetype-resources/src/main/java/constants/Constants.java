#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.constants;

public class Constants {
    //the slash at the beginning is important
    public static final String APP_CONTEXT_PATH = "/_APP_CONTEXT_PATH_";
}
