#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import ${package}.api.DefaultService;
import ${package}.constants.Constants;
import ${package}.dto.DummyDto;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.List;

@Component(service = DefaultController.class)
@Path(Constants.APP_CONTEXT_PATH) //always use this as prefix!
public class DefaultController {

  @Context
  UriInfo uriInfo;

  @Reference
  private DefaultService defaultService;

  @GET
  @Produces("application/json")
  public Response testController() {

    List<DummyDto> dummys = this.defaultService.getAllDummys();
    return Response
            .ok(dummys)
            .header("Access-Control-Allow-Origin", "*")
            .build();
  }
}
