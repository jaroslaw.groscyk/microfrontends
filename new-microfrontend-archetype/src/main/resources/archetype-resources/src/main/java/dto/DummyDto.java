#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.dto;

import java.util.Date;

public class DummyDto {

    private String message;
    private Date timestamp;

    public DummyDto() {
    }

    public DummyDto(String message, Date timestamp) {
        this.message = message;
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public DummyDto setMessage(String message) {
        this.message = message;
        return this;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public DummyDto setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
        return this;
    }
}
