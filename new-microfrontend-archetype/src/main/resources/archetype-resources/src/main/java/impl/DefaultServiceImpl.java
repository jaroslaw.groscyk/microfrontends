#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.impl;

import ${package}.api.DefaultService;
import ${package}.dto.DummyDto;
import org.osgi.service.component.annotations.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DefaultServiceImpl implements DefaultService {

    public List<DummyDto> getAllDummys() {
        List<DummyDto> dummys = new ArrayList<>();

        for (int i = 0; i <= 10; i ++) {
            dummys.add(new DummyDto(String.format("Hello from Dummy %s", i), new Date()));
        }
        return dummys;
    }
}
