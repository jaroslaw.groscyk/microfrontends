package de.adesso.thesis.osgi.microfrontend.main.impl.jackson;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class JacksonFeatureActivator implements BundleActivator {

    private ServiceRegistration<?> registration;

    @Override
    public void start(final BundleContext context) throws Exception {
        final JacksonFeature feature = new JacksonFeature();
        this.registration = context.registerService(JacksonFeature.class.getName(), feature, null);
    }

    @Override
    public void stop(final BundleContext context) throws Exception {
        if (this.registration != null) {
            this.registration.unregister();
        }
    }

}

