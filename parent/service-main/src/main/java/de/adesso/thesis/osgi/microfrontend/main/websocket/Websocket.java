package de.adesso.thesis.osgi.microfrontend.main.websocket;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketFrame;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.api.extensions.Frame;
import org.osgi.service.component.annotations.Component;

@Component(service = Websocket.class)
@WebSocket
public class Websocket {
	public static Set<Session> peers = Collections.synchronizedSet(new HashSet<>());

	@OnWebSocketMessage
	public void onText(Session session, String message) throws IOException {
		for (Session s : peers) {
			try {
				// just echo the message
				s.getRemote().sendString("echo: " + message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@OnWebSocketConnect
	public void onConnect(Session session) throws IOException {
		peers.add(session);
	}

	@OnWebSocketClose
	public void onClose(Session session, int status, String reason) {
		peers.remove(session);
	}

	@OnWebSocketFrame
	public void onFrame(Session sessoin, Frame frame) {
		// TODO: are we interested in frames?
	}

	public static void sendMessage(String message) {
		if (!peers.isEmpty()) {
			for (Session s : peers) {
				try {
					s.getRemote().sendString(message);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			// no Session is to socket established. nothing to do here.
		}
	}
}
