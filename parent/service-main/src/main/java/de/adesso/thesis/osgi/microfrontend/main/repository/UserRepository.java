package de.adesso.thesis.osgi.microfrontend.main.repository;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;

import de.adesso.thesis.osgi.microfrontend.domain.User;

public class UserRepository implements AutoCloseable {

    private final EntityManager entityManager;

    public UserRepository(final EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public Optional<User> findById(final long id) {
        return Optional.ofNullable(this.entityManager.find(User.class, id));
    }
    
    public Optional<User> findByLoginName(final String loginName) {
        try {

            return Optional.of(this.entityManager.createNamedQuery(User.FIND_BY_LOGINNAME, User.class) //
                    .setParameter("loginName", loginName) //
                    .getSingleResult());

        } catch (final NoResultException e) {
            return Optional.empty();
        }
    }

    public List<User> findAll() {
        return this.entityManager.createNamedQuery(User.FIND_ALL, User.class).getResultList();
    }

    public void persist(final User article) {
        this.entityManager.persist(article);
    }

    public User merge(final User article) {
        return this.entityManager.merge(article);
    }

    public <R> R runInTransaction(final Supplier<R> supplierToExecute) {
        try {

            this.entityManager.getTransaction().begin();
            final R result = supplierToExecute.get();
            this.entityManager.getTransaction().commit();
            return result;

        } finally {

            // close the active transaction, if any exception occured, that is not caused by the EntityManager
            if (this.entityManager.getTransaction().isActive()) {
                this.entityManager.getTransaction().rollback();
            }
        }
    }

    
    
    /*
     * (non-Javadoc)
     *
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() {
        try {

            // close transaction, because EntityManager will not release persistence context if a transaction is open
            if (this.entityManager.getTransaction().isActive()) {
                this.entityManager.getTransaction().rollback();
            }

        } finally {
            this.entityManager.close();
        }
    }

}
