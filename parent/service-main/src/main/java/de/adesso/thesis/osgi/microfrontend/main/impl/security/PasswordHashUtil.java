
package de.adesso.thesis.osgi.microfrontend.main.impl.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Objects;
import java.util.Random;

import de.adesso.thesis.osgi.microfrontend.domain.User;



public class PasswordHashUtil {

    private static final String HASHING_ALGORITHM = "SHA-256";

    private PasswordHashUtil() {
        // private constructor that does not allow to instantiate this class, because this class contains static methods only.
    }

    /**
     * Checks if the given password matches the specified user.
     *
     * @param user
     *            the user to match against the specified password
     * @param password
     *            the password to match against the specified user
     * @return <code>true</code> in case the specified password matches the password previously set for the specified user
     *
     * @throws AuthenticationSystemException
     *             in case an internal error hashing the specified password occurred
     */
    public static boolean doesPasswordMatch(final User user, final String password) throws AuthenticationSystemException {
        Objects.requireNonNull(user);
        Objects.requireNonNull(password);

        return user.getPassword().equals(hashPassword(password, Base64.getDecoder().decode(user.getHashedPasswordSalt())));
    }

    /**
     * Replaces the password of the specified user by a PBKDF2 password hash using a randomly generated salt.
     *
     * @param user
     *            the user to hash the password for
     *
     * @throws AuthenticationSystemException
     *             in case an internal error hashing the specified password occurred
     */
    public static void hashPassword(final User user) throws AuthenticationSystemException {

        Objects.requireNonNull(user);
        Objects.requireNonNull(user.getPassword());

        final byte[] salt = new byte[32];
        new Random().nextBytes(salt);
        user.setPassword(hashPassword(user.getPassword(), salt));
        user.setHashedPasswordSalt(Base64.getEncoder().encodeToString(salt));
    }

    /**
     * Creates a PBKDF2 hash of the specified password using the specified salt.
     *
     * @param password
     *            the plain-text password to hash
     * @param salt
     *            the salt to include in the hash
     * @return a base 64 encoded representation of the hashed password
     *
     * @throws AuthenticationSystemException
     *             in case an internal error hashing the specified password occurred
     */
    public static String hashPassword(final String password, final byte[] salt) throws AuthenticationSystemException {

        Objects.requireNonNull(salt, "salt");
        Objects.requireNonNull(password, "password");

        try {

            final MessageDigest md = MessageDigest.getInstance(HASHING_ALGORITHM);
            md.update(salt);
            final byte[] bytes = md.digest(password.getBytes());
            final StringBuilder sb = new StringBuilder();
            for (byte b : bytes) {
                sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();

        } catch (NoSuchAlgorithmException e) {
            throw new AuthenticationSystemException("Failed to create password hash.", e);
        }
    }
}
