
package de.adesso.thesis.osgi.microfrontend.main.impl.security;

import java.security.Principal;
import java.util.Objects;

import javax.ws.rs.container.ContainerRequestContext;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

import com.eclipsesource.jaxrs.provider.security.AuthenticationHandler;
import com.eclipsesource.jaxrs.provider.security.AuthorizationHandler;

import de.adesso.thesis.osgi.microfrontend.domain.AuthenticatedUser;


@Component(service = { AuthenticationHandler.class, AuthorizationHandler.class })
public class SecurityHandler implements AuthenticationHandler, AuthorizationHandler {

    private static final String TOKEN = "token";

    private JwtUtil jwtUtil;

    @Activate
    public void activate(final BundleContext bundleContext) {
        this.jwtUtil = JwtUtil.getInstance(bundleContext);
    }

    @Override
    public boolean isUserInRole(final Principal user, final String role) {

        Objects.requireNonNull(user);
        Objects.requireNonNull(role);

        if (user instanceof AuthenticatedUser) {
            return ((AuthenticatedUser) user).getAssignedRole().equals(role);
        }

        return false;
    }

    @Override
    public Principal authenticate(final ContainerRequestContext requestContext) {
        final String jwt = requestContext.getHeaderString(TOKEN);
        if (jwt == null) {
            return null;
        }

        try {
            return this.jwtUtil.extractUserFromToken(jwt);
        } catch (AuthenticationSystemException e) {
//            if (this.LOG.isLoggable(LogLevel.ERROR)) {
//                this.LOG.error("Error processing user token during authentication.", e);
//            }
            return null;
        } 
    }

    @Override
    public String getAuthenticationScheme() {
        return null;
    }

}
