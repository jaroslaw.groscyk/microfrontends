package de.adesso.thesis.osgi.microfrontend.main.controller;

import java.util.Optional;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import de.adesso.thesis.osgi.microfrontend.domain.Article;
import de.adesso.thesis.osgi.microfrontend.main.api.ArticleService;
import de.adesso.thesis.osgi.microfrontend.main.impl.Constants;

@Component(service = ArticleController.class)
@Path(Constants.PATH_ARTICLES)
@Produces({ Constants.MEDIATYPE_APPLICATION_JSON })
public class ArticleController {
	
	@Reference
	private ArticleService articleService;
	
    @GET
    @Path(Constants.PARAMETER_ID)
    //@RolesAllowed({ Constants.USER_ROLE_ADMINISTRATOR, Constants.USER_ROLE_PROFESSIONAL })
    public Response getArticle(@PathParam(Constants.ID) final long id) {
        final Optional<Article> article = this.articleService.getArticle(id);
        return article.map(foundArticle -> Response.ok(foundArticle).build()).orElse(Response.status(Response.Status.NOT_FOUND).build());
    }
    
    @GET
    //@RolesAllowed({ Constants.USER_ROLE_ADMINISTRATOR, Constants.USER_ROLE_PROFESSIONAL })
    public Response getArticles() {
        return Response.ok(this.articleService.getArticles()).header("Access-Control-Allow-Origin", "*").build();
    }
    
    @POST
    @RolesAllowed({ Constants.USER_ROLE_ADMINISTRATOR, Constants.USER_ROLE_PROFESSIONAL })
    public Response saveArticle(Article article) {
    	this.articleService.saveArticle(article);
    	
        return Response.ok().build();
    }
    
    @PUT
    @RolesAllowed({ Constants.USER_ROLE_ADMINISTRATOR, Constants.USER_ROLE_PROFESSIONAL })
    public Response updateArticle(Article article) {
    	this.articleService.updateArticle(article);
    	
        return Response.ok().build();
    }
    
    

}
