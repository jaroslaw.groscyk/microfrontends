
package de.adesso.thesis.osgi.microfrontend.main.impl.security;

import java.util.Objects;
import java.util.Optional;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import de.adesso.thesis.osgi.microfrontend.domain.User;
import de.adesso.thesis.osgi.microfrontend.main.api.LoginHandlerService;
import de.adesso.thesis.osgi.microfrontend.main.api.UserService;



@Component
public class LoginHandler implements LoginHandlerService {

    @Reference
    private UserService userManagementService;

    private JwtUtil jwtUtil;

    @Activate
    public void activate(final BundleContext bundleContext) {
        this.jwtUtil = JwtUtil.getInstance(bundleContext);

    }

    @Override
    public User getUserByUsernameAndPassword(final String username, final String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        final Optional<User> userOptional = this.userManagementService.getUserByLoginName(username);

        if (userOptional.isPresent()) {
            final User user = userOptional.get();

            if (PasswordHashUtil.doesPasswordMatch(user, password)) {
                return user;
            }
        }

        throw new AuthenticationSystemException("Unknown user");
    }

    @Override
    public String getUserToken(final User user) {
        return this.jwtUtil.getJwtToken(user);
    }

    @Override
    public String getUserToken(final long userId) {
        final User user = this.userManagementService.getUser(userId)
                .orElseThrow(() -> new IllegalStateException("User not found with id " + userId));

        return this.getUserToken(user);
    }

}
