package de.adesso.thesis.osgi.microfrontend.main.websocket;

import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.wiring.BundleWiring;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.http.HttpService;

@Component(service = SocketServlet.class)
public class SocketServlet extends WebSocketServlet {

	@Reference
	private HttpService httpService;

	@Activate
	public void start(BundleContext context) {
		try {
			ClassLoader ccl = Thread.currentThread().getContextClassLoader();

			BundleWiring bundleWiring = (BundleWiring) findJettyBundle(context).adapt(BundleWiring.class);
			ClassLoader classLoader = bundleWiring.getClassLoader();
			Thread.currentThread().setContextClassLoader(classLoader);
			httpService.registerServlet("/websocket", this, null, httpService.createDefaultHttpContext());
			Thread.currentThread().setContextClassLoader(ccl);

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	private Bundle findJettyBundle(BundleContext m_bundleContext) {

		Bundle result = null;
		Bundle[] bundles = m_bundleContext.getBundles();
		for (int i = 0; i < bundles.length; i++) {
			if ("org.apache.felix.http.jetty".equals(bundles[i].getSymbolicName())) {
				result = bundles[i];
				break;
			}
		}
		return result;
	}

	@Override
	public void configure(WebSocketServletFactory factory) {
		factory.register(Websocket.class);
	}

}
