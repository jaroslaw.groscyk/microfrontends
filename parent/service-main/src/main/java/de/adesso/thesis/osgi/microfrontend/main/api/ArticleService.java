package de.adesso.thesis.osgi.microfrontend.main.api;


import java.util.List;
import java.util.Optional;

import de.adesso.thesis.osgi.microfrontend.domain.Article;

public interface ArticleService {

	List<Article> getArticles();
	
	Optional<Article> getArticle(Long id);
	
	void saveArticle(Article article);
	
	void updateArticle(Article article);
}
