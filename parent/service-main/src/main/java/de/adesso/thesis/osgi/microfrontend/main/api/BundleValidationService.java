package de.adesso.thesis.osgi.microfrontend.main.api;

import java.util.Dictionary;
import java.util.List;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import de.adesso.thesis.osgi.microfrontend.domain.MicroFrontend;

public interface BundleValidationService {
	public boolean isValidBundle(Dictionary<String, String> manifestHeaders);
	public void cleanUp(Dictionary<String, String> manifestHeaders);
	public void sendWebsocketMessage(String message, String microFrontendName);
}
