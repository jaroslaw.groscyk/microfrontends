package de.adesso.thesis.osgi.microfrontend.main.controller;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import de.adesso.thesis.osgi.microfrontend.domain.MicroFrontend;
import de.adesso.thesis.osgi.microfrontend.main.api.MicroFrontendService;

@Component(service = MicroFrontendController.class)
@Path("/micro-frontends")
@Produces("application/json")
public class MicroFrontendController {

	@Reference
	private MicroFrontendService microFrontendService;

	@GET
	public Response getMicroFrontends() {
		List<MicroFrontend> microfrondends = this.microFrontendService.getMicrofrondends();
		
		return Response.ok(microfrondends).header("Access-Control-Allow-Origin", "*").build();
	}
}
