
package de.adesso.thesis.osgi.microfrontend.main.impl.security;

import static de.adesso.thesis.osgi.microfrontend.main.impl.Constants.*;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Date;
import java.util.Objects;

import javax.security.auth.login.FailedLoginException;

import org.osgi.framework.BundleContext;

import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import de.adesso.thesis.osgi.microfrontend.domain.AuthenticatedUser;
import de.adesso.thesis.osgi.microfrontend.domain.Role;
import de.adesso.thesis.osgi.microfrontend.domain.User;
import de.adesso.thesis.osgi.microfrontend.main.impl.Constants;

/**
 * Utility used to work with JSON Web Tokens.
 *
 * @author Andreas Kliem andreas.kliem@adesso.de
 *
 */

public class JwtUtil {

    private final String tokenSignSecret;
    private final String tokenCryptSecret;
    private final String tokenIssuer;
    private final long tokenTTLInMillies;

    /**
     * Obtains a new JWT Util instance using the specified options
     *
     * @param tokenSignSecret
     *            the secret used to sign the token
     * @param tokenCryptSecret
     *            the secret used to encrypt the token
     * @param tokenIssuer
     *            the issuer of the token
     * @param tokenTTLInMillies
     *            the lifetime of the token in milliseconds
     */
    private JwtUtil(final String tokenSignSecret, final String tokenCryptSecret, final String tokenIssuer,
            final long tokenTTLInMillies) {
        super();
        this.tokenSignSecret = tokenSignSecret;
        this.tokenCryptSecret = tokenCryptSecret;
        this.tokenIssuer = tokenIssuer;
        this.tokenTTLInMillies = tokenTTLInMillies;
    }

    /**
     * Factory reading necessary configuration properties from the specified bundle context.
     *
     * @param bundleContext
     *            the bundle context to obtain the JWT configuration from
     * @return a {@link JwtUtil} instance operating using the obtained configuration
     * @throws NullPointerException
     *             in case a mandatory configuration property was not found
     * @throws NumberFormatException
     *             in case a mandatory configuration property could not be parsed
     */
    public static JwtUtil getInstance(final BundleContext bundleContext) throws NullPointerException, NumberFormatException {

        final String tokenSignSecret = getPropertyChecked(bundleContext, SECURITY_PROPERTY_TOKENSIGNSECRET);
        final String tokenCryptSecret = getPropertyChecked(bundleContext, SECURITY_PROPERTY_TOKENCRYPTSECRET);
        final String tokenIssuer = getPropertyChecked(bundleContext, SECURITY_PROPERTY_TOKENISSUER);
        final long tokenTTLInMillies = Long
                .parseLong(Objects.requireNonNull(getPropertyChecked(bundleContext, SECURITY_PROPERTY_TOKENTTL_MILLIES)));

        return new JwtUtil(tokenSignSecret, tokenCryptSecret, tokenIssuer, tokenTTLInMillies);
    }

    private static String getPropertyChecked(final BundleContext bundleContext, final String key) {
        return Objects.requireNonNull(bundleContext.getProperty(key), MESSAGE_CONFIG_PROPERTY_MISSING + key);
    }

    /**
     * Attempts to login the specified user. If successful, generates a new JSON web Token (JWT).
     *
     * @param user
     *            the user requesting the login
     * @return a {@link LoginResponse} containing the generated JWT
     * @throws FailedLoginException
     *             in case the provided password does not match the password stored in the DB
     * @throws JOSEException
     *             in case generating the JWT failed
     */
    public String getJwtToken(final User user) {
        Objects.requireNonNull(user);
        //logger.debug("New JWT for user %s requested.", user.getLoginName());
        final String jwt = this.createJwt(this.tokenIssuer, user.getLoginName(), String.valueOf(user.getId()), user.getRole(),
                this.tokenTTLInMillies);
        //logger.debug("Finished generating new JWT for user %s", user.getLoginName());
        return jwt;

    }

    /**
     * Utility operation to create an authenticated user instance based on the claims extracted from the specified token.
     *
     * @param jwt
     *            the jwt to extract the user instance from
     * @return a user instance providing the information encoded in the token
     * @throws AuthenticationSystemException
     *             if an internal error decoding the JWT occurred
     */
    public AuthenticatedUser extractUserFromToken(final String jwt) throws AuthenticationSystemException {
        //logger.debug("Starting to decrypt JWT %s", jwt);
        final JWTClaimsSet claims = this.extractClaimsFromToken(jwt);
        try {
            final AuthenticatedUser result = new AuthenticatedUser(Long.parseLong(claims.getSubject()),
                    claims.getStringClaim(Constants.SECURITY_JWT_CLAIM_KEYUSERNAME),
                    claims.getStringClaim(Constants.SECURITY_JWT_CLAIM_KEYUSERROLE));
            //logger.debug("Finished to decrypt JWT %s", jwt);
            return result;
        } catch (final ParseException e) {
            //logger.error("Failed to extract claims from jwt.", e);
            throw new AuthenticationSystemException("Failed to extract claims from jwt.", e);
        }

    }

    /**
     * Extracts claims encoded in the specified JWT.
     *
     * @param jwt
     *            the JWT to extract claims from
     * @return the claims encoded in the JWT
     * @throws AuthenticationSystemException
     *             if an internal error decoding the JWT occurred
     */
    public JWTClaimsSet extractClaimsFromToken(final String jwt) {

        final SignedJWT tokenPayload;
        final JWTClaimsSet tokenClaimSet;
        try {
            tokenPayload = this.parseTokenAndExtractPayload(jwt);
            tokenClaimSet = tokenPayload.getJWTClaimsSet();

            this.verifyTokenSignature(tokenPayload);
            this.verifyTokenValidity(tokenPayload);

            return tokenClaimSet;
        } catch (JOSEException | ParseException | FailedLoginException e) {
            //logger.error("Failed to extract payload from JWT: " + jwt, e);
            throw new AuthenticationSystemException("Failed to extract payload from JWT: " + jwt, e);
        }
    }

    private void verifyTokenValidity(final SignedJWT tokenPayload) throws FailedLoginException, ParseException {
        if ((this.tokenTTLInMillies != -1)
                && tokenPayload.getJWTClaimsSet().getExpirationTime().before(new Date(System.currentTimeMillis()))) {
            //logger.debug("JWT token for subject %s0 is expired.", tokenPayload.getJWTClaimsSet().getSubject());
            throw new FailedLoginException(
                    "JWT token for subject + " + tokenPayload.getJWTClaimsSet().getSubject() + " is expired.");
        }
    }

    private void verifyTokenSignature(final SignedJWT tokenPayload) throws FailedLoginException, JOSEException, ParseException {
        if (!tokenPayload.verify(new MACVerifier(this.tokenSignSecret))) {
            //logger.debug("JWT token for subject %s0 has invalid signature.", tokenPayload.getJWTClaimsSet().getSubject());
            throw new FailedLoginException(
                    "JWT token for subject " + tokenPayload.getJWTClaimsSet().getSubject() + " has invalid signature.");
        }
    }

    private SignedJWT parseTokenAndExtractPayload(final String jwt) throws JOSEException, ParseException {
        // Parse the JWE string
        final JWEObject jweObject = JWEObject.parse(jwt);

        // Decrypt with shared key
        jweObject.decrypt(new DirectDecrypter(this.tokenCryptSecret.getBytes(StandardCharsets.UTF_8)));

        // Extract payload
        return jweObject.getPayload().toSignedJWT();
    }

    private String createJwt(final String issuer, final String loginName, final String userId, final Role userRole,
            final long ttlMillis) {
        final SignedJWT sigendJwt = this.getSignedJwt(issuer, loginName, userId, userRole, ttlMillis);

        // Create JWE object with signed JWT as payload
        final JWEObject jweObject = new JWEObject(
                new JWEHeader.Builder(JWEAlgorithm.DIR, EncryptionMethod.A128GCM).contentType("JWT") // required to signal
                // nested JWT
                .build(),
                new Payload(sigendJwt));

        // Perform encryption
        try {
            jweObject.encrypt(new DirectEncrypter(this.tokenCryptSecret.getBytes(StandardCharsets.UTF_8)));
        } catch (final JOSEException e) {
            //logger.error("Failed to generate JWT for user.", e);
            throw new AuthenticationSystemException("Failed to generate JWT for user.", e);
        }

        // Serialise to JWE compact form
        return jweObject.serialize();

    }

    private SignedJWT getSignedJwt(final String issuer, final String loginName, final String userId, final Role userRole,
            final long ttlMillis) {
        // claims
        final JWTClaimsSet claimsSet = new JWTClaimsSet.Builder().subject(userId).claim(SECURITY_JWT_CLAIM_KEYUSERNAME, loginName)
                .claim(SECURITY_JWT_CLAIM_KEYUSERROLE, userRole.name())
                .expirationTime(new Date(System.currentTimeMillis() + ttlMillis)).issuer(issuer).build();

        // Sign
        final SignedJWT signedJwt = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);

        try {
            signedJwt.sign(new MACSigner(this.tokenSignSecret));
        } catch (final JOSEException e) {
            //logger.error("Failed to sign JWT for user.", e);
            throw new AuthenticationSystemException("Failed to sign JWT for user.", e);
        }

        return signedJwt;
    }

}
