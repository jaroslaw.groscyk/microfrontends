package de.adesso.thesis.osgi.microfrontend.main.impl;

import java.util.Date;
import java.util.Dictionary;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import de.adesso.thesis.osgi.microfrontend.domain.MicroFrontend;
import de.adesso.thesis.osgi.microfrontend.main.api.BundleValidationService;
import de.adesso.thesis.osgi.microfrontend.main.api.MicroFrontendService;
import de.adesso.thesis.osgi.microfrontend.main.websocket.Websocket;

@Component
public class BundleValidationServiceImpl implements BundleValidationService {

  @Reference
  private MicroFrontendService microFrontendService;

  @Override
  public boolean isValidBundle(Dictionary<String, String> manifestHeaders) {

    boolean isValidBundle = false;
    String message;
    String microFrontendName;
    String customElementName;
    String url;
    
    //check if required attributes are present
    if (manifestHeaders != null && !manifestHeaders.isEmpty()
        && (microFrontendName = manifestHeaders.get("Bundle-SymbolicName")) != null
        && (customElementName = manifestHeaders.get("Custom-Element-Name")) != null
        && (url = manifestHeaders.get("Url")) != null)
    {
      Optional<MicroFrontend> microFrontendFromDB = this.microFrontendService.getMicroFrontendByName(microFrontendName);
      if (microFrontendFromDB.isPresent()) {
        message = String.format("Microfrontend with name %s was already started", microFrontendName);
      }
      else {
        //create new microfrontend
        MicroFrontend newMicroFrontend = new MicroFrontend();
        newMicroFrontend.setName(microFrontendName);
        newMicroFrontend.setTagName(customElementName);
        newMicroFrontend.setCustomElement(String.format("<%s></%s>", customElementName, customElementName));
        newMicroFrontend.setApiContextPath(manifestHeaders.get("Api-Context-Path"));
        newMicroFrontend.setUrl(url);
        newMicroFrontend.setCreated(new Date().getTime());
        newMicroFrontend.setBundleVersion(manifestHeaders.get("Bundle-Version"));

        //persist
        this.microFrontendService.saveMicroFrontend(newMicroFrontend);

        isValidBundle = true;
        message = String.format("%s has been started", microFrontendName);
      }
    } else {
      message = "Could not validate Microfrontend";
    }
    Websocket.sendMessage(message);
    return isValidBundle;
  }

  @Override
  public void cleanUp(Dictionary<String, String> manifestHeaders) {
    String microFrontendName = manifestHeaders.get("Bundle-SymbolicName");

    Optional<MicroFrontend> result =
        this.microFrontendService.getMicroFrontendByName(microFrontendName);
    try {
      MicroFrontend microFrontend = result.get();
      this.microFrontendService.deleteMicroFrontend(microFrontend);
      Websocket.sendMessage(String.format("%s has been stopped", microFrontendName));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public void sendWebsocketMessage(String message, String microFrontendName) {
    Websocket.sendMessage(String.format(message, microFrontendName));
  }
}
