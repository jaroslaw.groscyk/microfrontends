package de.adesso.thesis.osgi.microfrontend.main.repository;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import de.adesso.thesis.osgi.microfrontend.domain.Article;
import de.adesso.thesis.osgi.microfrontend.domain.MicroFrontend;

public class MicroFrontendRepository implements AutoCloseable {
	private final EntityManager entityManager;

	public MicroFrontendRepository(final EntityManagerFactory entityManagerFactory) {
		this.entityManager = entityManagerFactory.createEntityManager();
	}

	public Optional<MicroFrontend> findByname(String name) {
		MicroFrontend result = null;

		Query query = this.entityManager.createQuery("select m from MicroFrontend m where m.name = :name", MicroFrontend.class);
		query.setParameter("name", name);
		try {
			result = (MicroFrontend) query.getSingleResult();
		} catch (NoResultException e) {

		}

		return Optional.ofNullable(result);
	}
	public List<MicroFrontend> findAll() {
		return this.entityManager.createNamedQuery(MicroFrontend.FIND_ALL, MicroFrontend.class).getResultList();
	}
	
	public void persist(final MicroFrontend microFrontend) {
        this.entityManager.persist(microFrontend);
    }

    public MicroFrontend merge(final MicroFrontend microFrontend) {
        return this.entityManager.merge(microFrontend);
    }

    public void remove(final MicroFrontend microFrontEnd) {
    	MicroFrontend current = null;
    	
    	if (!this.entityManager.contains(microFrontEnd)) {
    	    current = this.entityManager.merge(microFrontEnd);
    	}
    	this.entityManager.remove(current);
    }
    public <R> R runInTransaction(final Supplier<R> supplierToExecute) {
        try {

            this.entityManager.getTransaction().begin();
            final R result = supplierToExecute.get();
            this.entityManager.getTransaction().commit();
            return result;

        } finally {

            // close the active transaction, if any exception occured, that is not caused by the EntityManager
            if (this.entityManager.getTransaction().isActive()) {
                this.entityManager.getTransaction().rollback();
            }
        }
    }
	
	@Override
	public void close() throws Exception {
		try {

			// close transaction, because EntityManager will not release persistence context
			// if a transaction is open
			if (this.entityManager.getTransaction().isActive()) {
				this.entityManager.getTransaction().rollback();
			}

		} finally {
			this.entityManager.close();
		}

	}
}
