package de.adesso.thesis.osgi.microfrontend.main.controller;

import java.util.Optional;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import de.adesso.thesis.osgi.microfrontend.domain.User;
import de.adesso.thesis.osgi.microfrontend.main.api.LoginHandlerService;
import de.adesso.thesis.osgi.microfrontend.main.api.UserService;
import de.adesso.thesis.osgi.microfrontend.main.impl.Constants;
import de.adesso.thesis.osgi.microfrontend.main.impl.security.LoginRequest;

@Component(service = UserController.class)
@Path(Constants.PATH_USERS)
@Produces({ Constants.MEDIATYPE_APPLICATION_JSON })
public class UserController {
	
	@Reference
	private UserService userService;
	@Reference
	private LoginHandlerService loginHandler;
	
    @GET
    @Path(Constants.PARAMETER_ID)
    @RolesAllowed({ Constants.USER_ROLE_ADMINISTRATOR })
    public Response getUser(@PathParam(Constants.ID) final long id) {
        final Optional<User> article = this.userService.getUser(id);
        return article.map(foundArticle -> Response.ok(foundArticle).build()).orElse(Response.status(Response.Status.NOT_FOUND).build());
    }
    
    @GET
    public Response getUsers() {
    	System.out.println("-------------------------------------------------- in here");
        return Response.ok(this.userService.getUsers()).build();
    }
    
    @POST
    public Response saveUser(User user) {
    	this.userService.saveUser(user);
    	
        return Response.ok().build();
    }
    
    @PUT
    @RolesAllowed({ Constants.USER_ROLE_ADMINISTRATOR })
    public Response updateUser(User user) {
    	this.userService.updateUser(user);
    	
        return Response.ok().build();
    }
    
    @POST
    @Path(Constants.PATH_LOGIN)
    public Response login(final LoginRequest loginRequest) {
        final User user = this.loginHandler.getUserByUsernameAndPassword(loginRequest.getUsername(),
                loginRequest.getPassword());
        final String userToken = this.loginHandler.getUserToken(user);

        if ((user != null) && (userToken != null)) {
            return Response.ok(user).header(Constants.TOKEN, userToken).build();
        }

        return Response.status(Status.UNAUTHORIZED).build();
    }

}
