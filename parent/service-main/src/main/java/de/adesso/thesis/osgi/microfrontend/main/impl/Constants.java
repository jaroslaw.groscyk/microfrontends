package de.adesso.thesis.osgi.microfrontend.main.impl;

import org.osgi.service.jpa.EntityManagerFactoryBuilder;

public class Constants {

    /**
     * The persistence unit to use
     */
    public static final String PERSISTENCE_UNIT_NAME = "micro-frontend";

    /**
     * Is used as a filter to bind the entityManagerFactory corresponding to the intended persistence unit
     */
    public static final String PERSISTENCE_UNIT_OSGI_REFERENCE_FILTER = "(" + EntityManagerFactoryBuilder.JPA_UNIT_NAME + "="
            + PERSISTENCE_UNIT_NAME + ")";
	
	
    
    public final static String SLASH = "/";
    public final static String PARAMETER_BRACKETS_OPEN = "{";
    public final static String PARAMETER_BRACKETS_CLOSE = "}";
    public final static String GET = "GET";
    public final static String PUT = "PUT";
    
    public final static String ARTICLES = "articles";
    public final static String USERS = "users";
    public final static String LOGIN = "login";

    
    public final static String ID = "id";

    public final static String PARAMETER_ID = SLASH + PARAMETER_BRACKETS_OPEN + ID + PARAMETER_BRACKETS_CLOSE;
    
    public final static String USER_ROLE_ADMINISTRATOR = "ADMINISTRATOR";
    public final static String USER_ROLE_PROFESSIONAL = "PROFESSIONAL";
    
    
    public final static String PATH_ARTICLES = SLASH + ARTICLES;
    public final static String PATH_USERS = SLASH + USERS;
    public final static String PATH_LOGIN = SLASH + LOGIN;

    
    public final static String MEDIATYPE_APPLICATION_JSON = "application/json";
    
    public static final String TOKEN = "token";

    
    
    
    
    /**
     * The iterations to be performed by the password hashing algorithm
     */
    public static final int SECURITY_HASHING_ITERATIONS = 50000;

    /**
     * The key length used by the password hashing algorithm
     */
    public static final int SECURITY_HASHING_PBKEYSPEC_KEY_LENGTH = 256;

    /**
     * The secret key factory (hashing algorithm) to be used
     */
    public static final String SECURITY_HASHING_SECRETKEYFACTORY = "PBKDF2WithHmacSHA512";
  
    /**
     * The key of the user role encoded in the JWT
     */
    public static final String SECURITY_JWT_CLAIM_KEYUSERROLE = "userRole";

    /**
     * The key of the user role encoded in the JWT
     */
    public static final String SECURITY_JWT_CLAIM_KEYUSERNAME = "userName";
        
    /**
     * Property key for the secret used to sign tokens
     */
    public static final String SECURITY_PROPERTY_TOKENSIGNSECRET = "usermanagement.security.tokenSignSecret";

    /**
     * Property key for the secret used to encrypt tokens
     */
    public static final String SECURITY_PROPERTY_TOKENCRYPTSECRET = "usermanagement.security.tokenCryptSecret";

    /**
     * Property key for issuer of tokens
     */
    public static final String SECURITY_PROPERTY_TOKENISSUER = "usermanagement.security.tokenIssuer";

    /**
     * Property key for the token life time in milliseconds
     */
    public static final String SECURITY_PROPERTY_TOKENTTL_MILLIES = "usermanagement.security.tokenTTLInMillies";

    /**
     * Template used for errors caused by missing configuration properties.
     */
    public static final String MESSAGE_CONFIG_PROPERTY_MISSING = "Mandatory configuration property missing: ";
    
	private Constants() {
		
	}
}
