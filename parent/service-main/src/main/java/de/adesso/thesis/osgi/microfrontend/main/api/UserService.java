package de.adesso.thesis.osgi.microfrontend.main.api;

import java.util.List;
import java.util.Optional;

import de.adesso.thesis.osgi.microfrontend.domain.User;

public interface UserService {

	List<User> getUsers();
	
	Optional<User> getUser(Long id);
	
	Optional<User> getUserByLoginName(String loginName);
	
	void saveUser(User article);
	
	void updateUser(User article);
}
