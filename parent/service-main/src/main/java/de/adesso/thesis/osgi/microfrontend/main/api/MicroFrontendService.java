package de.adesso.thesis.osgi.microfrontend.main.api;

import java.util.List;
import java.util.Optional;

import de.adesso.thesis.osgi.microfrontend.domain.MicroFrontend;

public interface MicroFrontendService {

	List<MicroFrontend> getMicrofrondends();
	void saveMicroFrontend(MicroFrontend microFrontend);
	void deleteMicroFrontend(MicroFrontend microFrontend);
	Optional<MicroFrontend> getMicroFrontendByName(String name);

}
