
package de.adesso.thesis.osgi.microfrontend.main.api;

import de.adesso.thesis.osgi.microfrontend.domain.User;

public interface LoginHandlerService {

    User getUserByUsernameAndPassword(String username, String password);

    String getUserToken(User user);

    String getUserToken(long userId);

}
