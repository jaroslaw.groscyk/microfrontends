package de.adesso.thesis.osgi.microfrontend.main.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManagerFactory;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jpa.EntityManagerFactoryBuilder;

import de.adesso.thesis.osgi.microfrontend.domain.Article;
import de.adesso.thesis.osgi.microfrontend.main.api.ArticleService;
import de.adesso.thesis.osgi.microfrontend.main.repository.ArticleRepository;

@Component
public class ArticleServiceImpl implements ArticleService{
	
    private EntityManagerFactory entityManagerFactory;

    @Reference(target = Constants.PERSISTENCE_UNIT_OSGI_REFERENCE_FILTER)
    private void bindEntityManagerFactoryBuilder(final EntityManagerFactoryBuilder persistenceProvider) {
        this.entityManagerFactory = persistenceProvider.createEntityManagerFactory(Collections.emptyMap());
    }

	public List<Article> getArticles() {
        try (ArticleRepository repository = new ArticleRepository(this.entityManagerFactory)) {
            return repository.findAll();
        }
	}

	public Optional<Article> getArticle(Long id) {
        try (ArticleRepository repository = new ArticleRepository(this.entityManagerFactory)) {
            return repository.findById(id);
        }
	}

	public void saveArticle(Article article) {
        try (ArticleRepository repository = new ArticleRepository(this.entityManagerFactory)) {
        	repository.runInTransaction(() -> {

                repository.persist(article);
                return article;
            });
        }
		
	}

	public void updateArticle(Article article) {
        try (ArticleRepository repository = new ArticleRepository(this.entityManagerFactory)) {
        	repository.runInTransaction(() -> {

                return repository.merge(article);
            });
        }
		
	}

}
