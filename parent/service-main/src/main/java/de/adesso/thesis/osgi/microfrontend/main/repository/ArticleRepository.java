package de.adesso.thesis.osgi.microfrontend.main.repository;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import de.adesso.thesis.osgi.microfrontend.domain.Article;

public class ArticleRepository implements AutoCloseable {

	private final EntityManager entityManager;

	public ArticleRepository(final EntityManagerFactory entityManagerFactory) {
		this.entityManager = entityManagerFactory.createEntityManager();
	}

	public Optional<Article> findById(final long id) {
		return Optional.ofNullable(this.entityManager.find(Article.class, id));
	}

	public List<Article> findAll() {
		return this.entityManager.createNamedQuery(Article.FIND_ALL, Article.class).getResultList();
	}

	public void persist(final Article article) {
		this.entityManager.persist(article);
	}

	public Article merge(final Article article) {
		return this.entityManager.merge(article);
	}

	public <R> R runInTransaction(final Supplier<R> supplierToExecute) {
		try {

			this.entityManager.getTransaction().begin();
			final R result = supplierToExecute.get();
			this.entityManager.getTransaction().commit();
			return result;

		} finally {

			// close the active transaction, if any exception occured, that is not caused by
			// the EntityManager
			if (this.entityManager.getTransaction().isActive()) {
				this.entityManager.getTransaction().rollback();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() {
		try {

			// close transaction, because EntityManager will not release persistence context
			// if a transaction is open
			if (this.entityManager.getTransaction().isActive()) {
				this.entityManager.getTransaction().rollback();
			}

		} finally {
			this.entityManager.close();
		}
	}

}
