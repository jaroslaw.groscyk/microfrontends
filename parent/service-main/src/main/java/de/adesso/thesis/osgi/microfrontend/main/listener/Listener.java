package de.adesso.thesis.osgi.microfrontend.main.listener;

import java.util.Dictionary;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.service.component.annotations.Reference;
import de.adesso.thesis.osgi.microfrontend.main.api.BundleValidationService;
import de.adesso.thesis.osgi.microfrontend.main.websocket.Websocket;

public class Listener implements BundleActivator, BundleListener {

  @Reference
  private Websocket websocket;

  private BundleContext context;
  private BundleValidationService bundleValidationService;

  public void start(BundleContext context) {
    this.context = context;
    context.addBundleListener(this);
  }

  public void stop(BundleContext context) {
    context.removeBundleListener(this);
  }

  public void bundleChanged(BundleEvent event) {
    Bundle bundle = event.getBundle();
    Dictionary<String, String> manifestHeaders = bundle.getHeaders();
    String bundleType = manifestHeaders.get("Bundle-Type");
    String bundleName = manifestHeaders.get("Bundle-SymbolicName");
    if (bundleType != null && bundleType.equals("Micro-Frontend")) {
      this.bundleValidationService =
          this.context.getService(this.context.getServiceReference(BundleValidationService.class));
      if (event.getType() == BundleEvent.STARTED) {
        boolean isValidBundle = this.bundleValidationService.isValidBundle(manifestHeaders);
        
        if (!isValidBundle) {
          try {
            bundle.uninstall();
            this.bundleValidationService.sendWebsocketMessage("Could not load %s", bundleName);
          } catch (BundleException e) {
            e.printStackTrace();
          }
        }
      }
      if (event.getType() == BundleEvent.INSTALLED) {
        this.bundleValidationService.sendWebsocketMessage("%s has been started", bundleName);
      }
      if (event.getType() == BundleEvent.STOPPED) {
        this.bundleValidationService.cleanUp(manifestHeaders);
      }
    }
  }
}
