package de.adesso.thesis.osgi.microfrontend.main.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManagerFactory;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jpa.EntityManagerFactoryBuilder;

import de.adesso.thesis.osgi.microfrontend.domain.User;
import de.adesso.thesis.osgi.microfrontend.main.api.UserService;
import de.adesso.thesis.osgi.microfrontend.main.impl.security.PasswordHashUtil;
import de.adesso.thesis.osgi.microfrontend.main.repository.UserRepository;

@Component
public class UserServiceImpl implements UserService{

    private EntityManagerFactory entityManagerFactory;

    @Reference(target = Constants.PERSISTENCE_UNIT_OSGI_REFERENCE_FILTER)
    private void bindEntityManagerFactoryBuilder(final EntityManagerFactoryBuilder persistenceProvider) {
        this.entityManagerFactory = persistenceProvider.createEntityManagerFactory(Collections.emptyMap());
    }
    
	@Override
	public List<User> getUsers() {
        try (UserRepository repository = new UserRepository(this.entityManagerFactory)) {
            return repository.findAll();
        }
	}

	@Override
	public Optional<User> getUser(Long id) {
        try (UserRepository repository = new UserRepository(this.entityManagerFactory)) {
            return repository.findById(id);
        }
	}

	@Override
	public Optional<User> getUserByLoginName(String loginName) {
        try (UserRepository repository = new UserRepository(this.entityManagerFactory)) {
            return repository.findByLoginName(loginName);
        }
	}

	@Override
	public void saveUser(User user) {
        try (UserRepository repository = new UserRepository(this.entityManagerFactory)) {
        	repository.runInTransaction(() -> {

                // we need to hash the password an generate a salt
                PasswordHashUtil.hashPassword(user);
                repository.persist(user);
                return user;
            });
        }
		
	}

	@Override
	public void updateUser(User user) {
        try (UserRepository repository = new UserRepository(this.entityManagerFactory)) {
        	repository.runInTransaction(() -> {

                // we need to hash the password an generate a salt
                PasswordHashUtil.hashPassword(user);
                return repository.merge(user);
            });
        }
		
	}

}
