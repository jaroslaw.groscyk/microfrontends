package de.adesso.thesis.osgi.microfrontend.main.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManagerFactory;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jpa.EntityManagerFactoryBuilder;

import de.adesso.thesis.osgi.microfrontend.domain.MicroFrontend;
import de.adesso.thesis.osgi.microfrontend.main.api.MicroFrontendService;
import de.adesso.thesis.osgi.microfrontend.main.repository.MicroFrontendRepository;

@Component
public class MicroFrontendServiceImpl implements MicroFrontendService {

	private EntityManagerFactory entityManagerFactory;

	@Reference(target = Constants.PERSISTENCE_UNIT_OSGI_REFERENCE_FILTER)
	private void bindEntityManagerFactoryBuilder(final EntityManagerFactoryBuilder persistenceProvider) {
		this.entityManagerFactory = persistenceProvider.createEntityManagerFactory(Collections.emptyMap());
	}

	@Override
	public List<MicroFrontend> getMicrofrondends() {
		try (MicroFrontendRepository repository = new MicroFrontendRepository(this.entityManagerFactory)) {
			return repository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void saveMicroFrontend(MicroFrontend microFrontend) {
		try (MicroFrontendRepository repository = new MicroFrontendRepository(this.entityManagerFactory)) {
			repository.runInTransaction(() -> {
				repository.persist(microFrontend);
				return microFrontend;
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteMicroFrontend(MicroFrontend microFrontend) {
		try (MicroFrontendRepository repository = new MicroFrontendRepository(this.entityManagerFactory)) {
			repository.runInTransaction(() -> {
				repository.remove(microFrontend);
				return microFrontend;
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Optional<MicroFrontend> getMicroFrontendByName(String name) {
		try (MicroFrontendRepository repository = new MicroFrontendRepository(this.entityManagerFactory)) {
			return repository.findByname(name);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
