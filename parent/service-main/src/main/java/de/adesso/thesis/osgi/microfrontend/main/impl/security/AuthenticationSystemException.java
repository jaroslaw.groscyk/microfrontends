/*
 *******************************************************************************
 *
 * (C) All rights reserved by BOSCH HEALTHCARE SOLUTIONS GMBH, WAIBLINGEN
 *
 *******************************************************************************
 *
 *    __   __   ___  ___
 *   /_ / /  / /__  /    /__/
 *  /__/ /__/ __ / /__  /  /
 *
 *
 *******************************************************************************
 * Administrative Information (automatically filled in)
 *
 * @ Project: user-management-service-impl
 * @ File Name: AuthenticationException.java
 * @ URL: http://www.bosch.de/
 * @ Author: Andreas Kliem <Andreas Kliem@de.bosch.com>
 * @ Date: 28.06.2018 18:14:07
 *
 * $Revision$
 * $LastChangedBy$
 * $LastChangedDate$
 *
 */
package de.adesso.thesis.osgi.microfrontend.main.impl.security;

/**
 * Exception used to cover exceptions thrown due to (cryption) errors in the authentication subsystem.
 *
 * @author Andreas Kliem andreas.kliem@adesso.de
 *
 */

public class AuthenticationSystemException extends RuntimeException {

    private static final long serialVersionUID = 2230324900863575725L;

    /**
     *
     * @param message
     *            the detail message
     * @param cause
     *            the cause of the exception
     */
    public AuthenticationSystemException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     *            the detail message
     */
    public AuthenticationSystemException(final String message) {
        super(message);
    }

}
