import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';

import {Article} from '../../models/article.interface';
import {ArticleDashboardService} from '../../article-dashboard.service';
import {Subject, Observable, Subscription} from 'rxjs';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Component({
  selector: 'article-dashboard',
  styleUrls: ['article-dashboard.component.scss'],
  template:
      `
    <div>{{ message }}</div>
    <div [innerHtml]="template"></div>
  `
})
export class ArticleDashboardComponent implements OnInit {
  private socket: Subject<any>;

  public message: string;
  articles: Article[];

  template: any;

  constructor(private articleService: ArticleDashboardService) {
    this.socket = articleService.createWebsocket();
  }

  ngOnInit() {
    this.socket.subscribe(
      message => {
        this.message = message.data;

        this.articleService
          .getArticles(this.message + '/index.html')
          .subscribe((htmlFile: any) => {
            console.log(htmlFile);
            this.template = '<div>hallo</div>';



            // //import javascript
            // const script = document.createElement('script');
            // script.type = 'text/javascript';
            // script.src = 'http://localhost:12000' + this.message + '/script.js';
            // document.getElementsByTagName('head')[0].appendChild(script);
            // //import css
            // const css = document.createElement('link');
            // css.rel = 'stylesheet';
            // css.href = 'http://localhost:12000' + this.message + '/style.css';
            // document.getElementsByTagName('head')[0].appendChild(css);


          });
      });
  }
}
