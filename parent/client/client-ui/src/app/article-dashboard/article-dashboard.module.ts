import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import { ArticleDashboardService } from './article-dashboard.service';
import { ArticleDashboardComponent } from './containers/article-dashboard/article-dashboard.component';
import { ArticleDetailComponent } from './components/article-detail/article-detail.component';
@NgModule({
    declarations: [
        ArticleDashboardComponent,
        ArticleDetailComponent
    ],
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        MaterialModule,
        HttpModule
    ],
    exports: [
        ArticleDashboardComponent
    ],
    providers: [
        ArticleDashboardService
    ]
})
export class ArticleDashboardModule {}
