import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Subject, Observer, Observable} from 'rxjs';

import {map} from 'rxjs/operators';

const ARTICLE_API = 'http://localhost:12000/services/articles';
const my_api = 'http://localhost:12000';

@Injectable()
export class ArticleDashboardService {

  createAuthorizationHeader(headers: Headers) {
    headers.append('Access-Control-Allow-Origin', '*');
  }

  constructor(private http: Http) {
    console.log(http);
  }

  public createWebsocket(): Subject<MessageEvent> {
    const socket = new WebSocket('ws://localhost:12000/websocket');
    const observable = Observable.create(
      (observer: Observer<MessageEvent>) => {
        socket.onmessage = observer.next.bind(observer);
        socket.onerror = observer.error.bind(observer);
        socket.onclose = observer.complete.bind(observer);
        return socket.close.bind(socket);
      }
    );
    const observer = {
      next: (data: Object) => {
        if (socket.readyState === WebSocket.OPEN) {
          socket.send(JSON.stringify(data));
        }
      }
    };
    return Subject.create(observer, observable);
  }

  getArticles(uri: String): any {
    return this.http
      .get(my_api + uri)
      .pipe(map((response: Response) => {
        // console.log(response.json());
        // return response.json();

        console.log(response);
        return response.text();

      }));
  }


}
