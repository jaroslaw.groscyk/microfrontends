import { Input, Component } from '@angular/core';

import { Article } from '../../models/article.interface';

@Component({
    selector: 'article-detail',
    styleUrls: ['article-detail.component.scss'],
    templateUrl: './article-detail.component.html'
})
export class ArticleDetailComponent {

    @Input()
    detail: Article;

    onSelect(article: Article): void {
        alert(article.name);
    }
}
