import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {MatDrawer, MatSnackBar} from '@angular/material';
import {Subject} from 'rxjs';
import {ContainerWebsocketService} from './container.websocket.service';
import {Microfrontend} from '../article-dashboard/models/microfrontend.interface';
import {ContainerRestService} from './container.rest.service';
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'container-component',
  styleUrls: ['./container.component.css'],
  templateUrl: './container.component.html'
})
export class ContainerComponent implements OnInit {
  public title = 'Microfronted Parent UI';
  public microFrontends: Microfrontend[] = [];
  private socket: Subject<any>;
  private message: string;
  private activeMicrofrontend: Microfrontend;

  constructor(private appContainerWebsocketService: ContainerWebsocketService,
              private appContainerRestService: ContainerRestService,
              public snackBar: MatSnackBar) {
    this.socket = appContainerWebsocketService.createWebsocket();
  }

  @ViewChild('remoteComponentContainer')
  private remoteComponentContainer: ElementRef;

  @ViewChild('drawer')
  public drawer: MatDrawer;

  @Input()
  microFrontend: Microfrontend;

  public closeDrawer(): void {
    this.drawer.close();
  }

  onSelect(microFrontend: Microfrontend): void {
    this.activeMicrofrontend = microFrontend;
    this.drawer.open();
  }


  ngOnInit(): void {
    // Initially, get all microfrontends from REST-Endpoint
    this.getMicroFrontendsFromRestEndpoint();

    //subscribe to websocket and listen to changes
    this.socket.subscribe(
      message => {
        // Something in the backend changed -> can be a 'microfrontend started' or 'microfrontend stopped'
        // is just used to visually inform the user. In both cases a fresh list of microfrontends will
        // be fetched via REST-call.
        this.message = message.data;

        // remove all microfrontend scripts in the container
        const scriptContainer = document.getElementById('script-container');
        scriptContainer.innerHTML = '';

        // notify the UI about changes
        this.snackBar.open(this.message, 'close', {
          duration: 2000,
          horizontalPosition: 'end',
          verticalPosition: 'top',
        });
        // get actual list of microfrontends
        this.getMicroFrontendsFromRestEndpoint();
      });
  }


  private getMicroFrontendsFromRestEndpoint(): void {
    this.microFrontends = [];

    const scriptContainer = document.getElementById('script-container');
    this.appContainerRestService.getMicroFrontends().subscribe((data: any) => {

      // update the list of active microfrontends from the backend
      this.microFrontends = data;

      // for all microfrontends from the REST-call, create a script-tag and append
      // it to the script-container
      for (let i = 0; i < this.microFrontends.length; i++) {

        const script = document.createElement('script');

        script.type = 'text/javascript';
        script.src = this.microFrontends[i].url;

        scriptContainer.appendChild(script);
      }

      // if the drawer is open and the active microfrontend is no longer in the result list,
      // then close the drawer
      if (this.drawer.opened && !this.microFrontends.some(m => m.name === this.activeMicrofrontend.name)) {
        this.drawer.close();
      }
    },
      (error: HttpErrorResponse) => {
      console.info(error.status);
      if (error.status === 503) {
        this.getMicroFrontendsFromRestEndpoint();
      }
      });
  }
}
