export interface Microfrontend {
  id: number;
  name: string;
  url: string;
  customElement: string;
  created: number;
  bundleVersion: boolean;
  newTag: string;
}
