import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  template: //html
  `
<container-component></container-component>
  `
})
export class AppComponent {
  title = 'Article Dashboard App';
}
