import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ContainerRestService {
  constructor(private http: HttpClient) { }

  microFrontendsUrl = 'http://localhost:12000/services/micro-frontends';

  public getMicroFrontends() {
    return this.http.get(this.microFrontendsUrl);
  }
}
