import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
// import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import {ContainerComponent} from './container.component';
import {ContainerWebsocketService} from './container.websocket.service';
import {ContainerRestService} from './container.rest.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {SanitizeHtmlPipe} from './app.sanitizehtmlpipe';

@NgModule({
  declarations: [
    ContainerComponent,
    SanitizeHtmlPipe
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    // HttpModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    ContainerComponent
  ],
  providers: [
    ContainerWebsocketService,
    ContainerRestService
  ]
})
export class ContainerModule {}
