package de.adesso.thesis.osgi.microfrontend.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({ //
		@NamedQuery(name = Article.FIND_ALL, query = Article.QUERY_FIND_ALL), })
public class Article {

	public static final String FIND_ALL = "Article.findAll";
	public static final String QUERY_FIND_ALL = "SELECT o FROM Article o";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(unique = true, nullable = false, updatable = false)
	private String name;

	@Column(nullable = true)
	private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
