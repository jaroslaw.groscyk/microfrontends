
package de.adesso.thesis.osgi.microfrontend.domain;

import java.security.Principal;
import java.util.Objects;


public class AuthenticatedUser implements Principal {

    private final long userId;
    private final String name;
    private final String assignedRole;

    /**
     * @param userName
     * @param assignedRoles
     */
    public AuthenticatedUser(final long userId, final String userName, final String assignedRole) {

        Objects.requireNonNull(userName);
        Objects.requireNonNull(assignedRole);

        this.userId = userId;
        this.name = userName;
        this.assignedRole = assignedRole;
    }

    public long getUserId() {
        return this.userId;
    }

    @Override
    public String getName() {
        return this.name;
    }

    /**
     * @return the set of roles assigned to the user.
     */
    public String getAssignedRole() {
        return this.assignedRole;
    }

}
