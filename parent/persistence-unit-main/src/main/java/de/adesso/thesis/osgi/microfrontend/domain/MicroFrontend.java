package de.adesso.thesis.osgi.microfrontend.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({ //
        @NamedQuery(name = MicroFrontend.FIND_ALL, query = MicroFrontend.QUERY_FIND_ALL),})
public class MicroFrontend {
    public static final String FIND_ALL = "MicroFrontend.findAll";
    public static final String QUERY_FIND_ALL = "SELECT o FROM MicroFrontend o";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true, nullable = false, updatable = false)
    private String name;

    @Column(unique = true, nullable = true)
    private String apiContextPath;

    @Column(unique = true, nullable = false)
    private String url;

    @Column(unique = true, nullable = false)
    private String tagName;

    @Column(unique = true, nullable = false)
    private String customElement;

    @Column(unique = false, nullable = true)
    private String bundleVersion;

    @Column(unique = false, nullable = false)
    private long created;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApiContextPath() {
        return apiContextPath;
    }

    public MicroFrontend setApiContextPath(String contextPath) {
        this.apiContextPath = contextPath;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getCustomElement() {
        return customElement;
    }

    public MicroFrontend setCustomElement(String customElement) {
        this.customElement = customElement;
        return this;
    }

    public String getBundleVersion() {
        return bundleVersion;
    }

    public MicroFrontend setBundleVersion(String bundleVersion) {
        this.bundleVersion = bundleVersion;
        return this;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public static String getFindAll() {
        return FIND_ALL;
    }

    public static String getQueryFindAll() {
        return QUERY_FIND_ALL;
    }

}
