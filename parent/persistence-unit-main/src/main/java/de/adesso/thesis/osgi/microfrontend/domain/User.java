package de.adesso.thesis.osgi.microfrontend.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


import org.eclipse.persistence.annotations.Index;

@Entity
@Index(name = "USER_INDEX", columnNames = { "id", "loginName" })
@NamedQueries({ //
    @NamedQuery(name = User.FIND_ALL, query = User.QUERY_FIND_ALL),
    @NamedQuery(name = User.FIND_BY_LOGINNAME, query = User.QUERY_FIND_BY_LOGINNAME),
    @NamedQuery(name = User.FIND_BY_ID, query = User.QUERY_FIND_BY_ID) })
public class User {

    public static final String FIND_ALL = "User.findAll";
    public static final String QUERY_FIND_ALL = "SELECT u FROM User u";

    public static final String FIND_BY_ID = "User.findById";
    public static final String QUERY_FIND_BY_ID = "SELECT u FROM User u WHERE u.id = :id";

    public static final String FIND_BY_LOGINNAME = "User.findByLoginName";
    public static final String QUERY_FIND_BY_LOGINNAME = "SELECT u FROM User u WHERE u.loginName = :loginName";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 255)
    private String loginName;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String hashedPasswordSalt;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Role role;


    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + (int) (this.id ^ (this.id >>> 32));
        result = (prime * result) + ((this.loginName == null) ? 0 : this.loginName.hashCode());
        result = (prime * result) + ((this.password == null) ? 0 : this.password.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.loginName == null) {
            if (other.loginName != null) {
                return false;
            }
        } else if (!this.loginName.equals(other.loginName)) {
            return false;
        }
        if (this.password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!this.password.equals(other.password)) {
            return false;
        }
        return true;
    }


    /**
     * @return the id
     */
    public long getId() {
        return this.id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * @return the loginName
     */
    public String getLoginName() {
        return this.loginName;
    }

    /**
     * @param loginName
     *            the loginName to set
     */
    public void setLoginName(final String loginName) {
        this.loginName = loginName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the salt used to hash the password
     */
    public String getHashedPasswordSalt() {
        return this.hashedPasswordSalt;
    }

    /**
     * @param hashedPasswordSalt
     *            the salt to set
     */
    public void setHashedPasswordSalt(final String hashedPasswordSalt) {
        this.hashedPasswordSalt = hashedPasswordSalt;
    }

    /**
     * @return the role
     */
    public Role getRole() {
        return this.role;
    }

    /**
     * @param role
     *            the role to set
     */
    public void setRole(final Role role) {
        this.role = role;
    }
}
