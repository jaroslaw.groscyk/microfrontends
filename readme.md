# Microfrontends with Angular and OSGi

This prototype shows how to include standalone Angular Applications as Micro-Frontends within a Parent User-Uinterface using Web Components in an OSGi enviroment. 

## Getting Started
### Prerequisites

* Maven
* Node 8
* Java 8 (for running apache felix- you can build your microfrontends with higher JDKs)

### Run the OSGi Enviroment
In the **/parent** folder run:
```
mvn clean install
```
In **/parent/osgi-runtime/felix-framework-5.6.10** run:
```
java -jar bin/felix.jar 
```
Check the OSGi console [OSGi Console](http://localhost:12000/system/console)(USER: admin, PW: 123456) and the User Interface [Client UI](http://localhost:12000/client-ui/index.html). 
### Include a MicroFrontend (do so for microFrontendA and microFrontendB)
In the **/microFrontendA** folder run:
```
mvn clean install
```
A new Bundle has been depoley in **/microFrontendA/target/microFrontendA-0.0.1-SNAPSHOT.jar**.
Now, start the new Bundle by typing in the Felix-OSGi Shell:
```
start file:/{pathToRepo}/microFrontendA/target/microFrontendA-0.0.1-SNAPSHOT.jar
```
Now see the the MicroFrontend included in the [Client UI](http://localhost:12000/client-ui/index.html) as Custom Element.
## Building a new Microfrontend
In order to create new Microfrontends, that you can place in our Client-UI, you'll need to create a new Angular Application and build it as a Web Component. In addition to that, a OSGi Bundle is needed, that connects to the existing Enviroment and serves that component.
You can do this by using the **new-microfrontend-archetype**.
### install the archetype

in the **/new-microfrontend-archetype** run 
```
mvn clean install
```
after that, you can create new micro-frontends with
```
mvn archetype:generate \
        -DarchetypeGroupId=de.adesso.thesis.osgi.microfrontend \
        -DarchetypeArtifactId=new-microfrontend-archetype \
        -DarchetypeVersion=0.0.2-SNAPSHOT \
        -DgroupId=de.adesso.thesis.osgi.microfrontend \
        -DartifactId=microFrontendX \
        -Dversion=0.0.1-SNAPSHOT
```
### Build and Run
After confirming your settings, change to the new directory an build your new component running:
```
mvn clean install
```
Start the new generated .jar-file in the your OSGi-shell:
```
start file:/microFrontendX/target/microFrontendX-0.0.1-SNAPSHOT.jar
```
## Communication between the micro-frontends
you can send a message to another microfrontend by setting the name of the microfrontend as the target. (eg you deployed a microfrontend with the name microFrontendX, set microFrontentd as the target)

