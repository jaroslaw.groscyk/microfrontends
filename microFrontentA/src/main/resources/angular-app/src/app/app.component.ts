import {Component, ElementRef, Input, ViewEncapsulation, OnInit, HostListener} from '@angular/core';
import {EventBusMessage} from "./models/event.bus.message.interface";
import {MatTableDataSource} from "@angular/material";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class AppComponent implements OnInit {
  title = 'microFrontentA-module';
  name = 'Angular';

   @Input() attributeFromParent: string;
  private nativeElement: any;
  public receivedMessages: EventBusMessage[] = [];
  public dataSource;
  public unknownElementError: boolean;
  displayedColumns = ['sender', 'receiver', 'message', 'timestamp'];

  constructor(private  elementRef: ElementRef) {
    this.attributeFromParent = this.elementRef.nativeElement.getAttribute('attributeFromParent');
    this.nativeElement = elementRef.nativeElement;
    this.dataSource = new MatTableDataSource<EventBusMessage>(this.receivedMessages);

  }
  
  ngOnInit() {
  }

  sendMessage(target: string, message: string): void {
    let newMessage: EventBusMessage = {
      sender: this.nativeElement.tagName.toLowerCase(),
      receiver: target,
      message: message,
      timestamp: new Date()
   }

    let messagTatget = target.toLocaleLowerCase() + '-module';
    let event = new CustomEvent('messageEvent', { detail: newMessage, bubbles: true });
    let targetElement = document.querySelector(messagTatget);
    try {
      targetElement.dispatchEvent(event);
      this.unknownElementError = false;
    }
    catch (exception) {
      console.info(exception);
      this.unknownElementError = true;
    }
  }
  @HostListener('messageEvent', ['$event.detail'])
  handleMessage(message: EventBusMessage) {
    console.info(message.message);

    this.receivedMessages.push(message);

    this.dataSource._updateChangeSubscription();
  }
}
