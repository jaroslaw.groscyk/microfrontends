package de.adesso.thesis.osgi.microfrontend.impl;

import de.adesso.thesis.osgi.microfrontend.api.DefaultService;
import de.adesso.thesis.osgi.microfrontend.dto.DummyDto;
import org.osgi.service.component.annotations.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DefaultServiceImpl implements DefaultService {

    public List<DummyDto> getAllDummys() {
        List<DummyDto> dummys = new ArrayList<>();

        for (int i = 0; i <= 10; i ++) {
            dummys.add(new DummyDto(String.format("Hello from Dummy %s", i), new Date()));
        }
        return dummys;
    }
}
