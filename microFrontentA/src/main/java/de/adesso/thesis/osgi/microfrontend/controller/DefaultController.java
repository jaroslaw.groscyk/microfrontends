package de.adesso.thesis.osgi.microfrontend.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import de.adesso.thesis.osgi.microfrontend.api.DefaultService;
import de.adesso.thesis.osgi.microfrontend.dto.DummyDto;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.List;

@Component(service = DefaultController.class)
//@Path("/microfrontenda")
@Produces("application/json")
public class DefaultController {

  @Reference
  private DefaultService defaultService;

  @GET
  public Response testController() {

    List<DummyDto> dummys = this.defaultService.getAllDummys();
    return Response
            .ok(dummys)
            .header("Access-Control-Allow-Origin", "*")
            .build();
  }
}
